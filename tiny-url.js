(function (undefined) {
    function isUrl(url) {
        // Regex from https://stackoverflow.com/a/3809435, with a modification to allow for TLDs of up to 24 characters
        return /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,24}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)+$/
            .test(url);
    }

    var location = window.location;
    var segments = location.pathname.split("/");
    var issueNumber = segments[segments.length - 1];
    var baseUrl = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
    var fallback = baseUrl + "/" + (segments.length <= 1 ? "" : segments[segments.length - 2]);

    function redirect(issueLink) {
        var xhr = new XMLHttpRequest();

        xhr.onload = function () {
            try {
                var payload = JSON.parse(xhr.response);
                var message = payload.message;
                var title = payload.title;

                // Workaround IE 11 lack of support for new URL()
                var anchor = document.createElement("a");
                anchor.setAttribute("href", title);

                // Invalid URLs includes invalid issue numbers, issue titles that are not URLs, and recursive destination URLs
                var isInvalidUrl = message === "Not Found" || !isUrl(title) || anchor.hostname === location.hostname;
                if (isInvalidUrl) {
                    location.replace(fallback);
                } else {
                    location.replace(title);
                }
            } catch (e) {
                location.replace(fallback);
            }
        };

        xhr.onerror = function () {
            location.replace(fallback);
        };
        xhr.open("GET", issueLink + "/" + issueNumber);
        xhr.send();
    }

    window.tinyUrl = {
        redirect: redirect,
        redirectOnGithub: function (repo) {
            redirect("https://api.github.com/repos/" + repo + "/issues/");
        },
        redirectOnGitlab: function (repo) {
            redirect("https://gitlab.com/api/v4/projects/" + encodeURIComponent(repo) + "/issues");
        }
    };
})();
